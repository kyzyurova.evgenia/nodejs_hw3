const jwt = require('jsonwebtoken');

const authMiddleware = (req, res, next) => {
  const  {
    authorization
  } = req.headers;

  if (!authorization) {
    return res.status(401).json({ 'message': 'Please, provide authorization header' });
  }

  const [, jwt_token] = authorization.split(' ');

  if (!jwt_token) {
    return res.status(401).json({ 'message': 'Please, include token to request' });
  }
  try {
    const tokenPayload = jwt.verify(jwt_token, 'secretTokenKey');
    req.user = {
      userId: tokenPayload.userId,
      email: tokenPayload.email,
      role: tokenPayload.role
    }
    next();
  } catch (err) {
    return res.status(401).json({message: err.message});
  }

}

const driverMiddleware = (req, res, next) => {
  const  {
    authorization
  } = req.headers;

  if (!authorization) {
    return res.status(401).json({ 'message': 'Please, provide authorization header' });
  }

  const [, jwt_token] = authorization.split(' ');

  if (!jwt_token) {
    return res.status(401).json({ 'message': 'Please, include token to request' });
  }
  try {
    const tokenPayload = jwt.verify(jwt_token, 'secretTokenKey');
    req.user = {
      userId: tokenPayload.userId,
      email: tokenPayload.email,
      role: tokenPayload.role
    }
    if(req.user.role === 'DRIVER'){
      next();
    }else{
      throw new Error('You are not a DRIVER');
    }
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
}

const shipperMiddleware = (req, res, next) => {
  const  {
    authorization
  } = req.headers;

  if (!authorization) {
    return res.status(401).json({ 'message': 'Please, provide authorization header' });
  }

  const [, jwt_token] = authorization.split(' ');

  if (!jwt_token) {
    return res.status(401).json({ 'message': 'Please, include token to request' });
  }
  try {
    const tokenPayload = jwt.verify(jwt_token, 'secretTokenKey');
    req.user = {
      userId: tokenPayload.userId,
      email: tokenPayload.email,
      role: tokenPayload.role
    }
    if(req.user.role === 'SHIPPER'){
      next();
    }else{
      throw new Error('You are not a SHIPPER');
    }
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
}

module.exports = {
  authMiddleware,
  driverMiddleware,
  shipperMiddleware
}
