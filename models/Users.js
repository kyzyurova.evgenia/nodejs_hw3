const mongoose = require('mongoose');
const Joi = require('joi');

const userJoiSchema = Joi.object({
  role: Joi.string()
    .pattern(new RegExp('^(DRIVER|SHIPPER)$')),

  email: Joi.string()
    .pattern(new RegExp('[a-zA-Z0-9._%+-]+@[a-z]+.[a-z]{2,}')),
    

  password: Joi.string()
    .alphanum()
    .min(8)
    .max(30)
    .required(),
    
});

const User = mongoose.model('User', {
  role: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  created_date: {
    type: Date, 
    default: Date.now(),
  },
});

module.exports = {
  User,
  userJoiSchema
};
