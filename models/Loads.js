const mongoose = require('mongoose');

const loadSchema = mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    default: null
  },
  status: {
    type: String,
    default: 'NEW'
  },
  state: {
    type: String,
    default: 'Waiting for driver'
  },
  name: {
    type: String,
    required: true
  },
  payload: {
    type: Number,
    required: true
  },
  pickup_address: {
    type: String,
    required: true
  },
  delivery_address: {
    type: String,
    required: true
  },
  dimensions: {
    type: Object,
    required: true
  },
  logs: {
    type: Array,
    default: []
  },
  created_date: {
    type: Date, 
    default: Date.now(),
  },
});

const Load = mongoose.model('load', loadSchema);

module.exports = {
  Load,
};
