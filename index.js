const fs = require('fs');
const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://evgeniaAdmin:m0n9b8v7c6x5z4@todoapp.m6jlf3v.mongodb.net/?retryWrites=true&w=majority');

const { authRouter } = require('./routers/authRouter.js');
const { usersRouter } = require('./routers/usersRouter.js');
const { loadsRouter } = require('./routers/loadsRouter.js');
const { trucksRouter } = require('./routers/trucksRouter.js');


app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);
app.use('/api/loads', loadsRouter);
app.use('/api/trucks', trucksRouter);


const start = async () => {
  try {
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res, next) {
  res.status(500).send({ message: err.message });
}
