const express = require('express');
const router = express.Router();
const {addTruck, getTrucks, getTruck, updateTruck, deleteTruck, assignTruck} = require('../controllers/trucksController.js');
const { authMiddleware, driverMiddleware } = require('../middleware/authMiddleware');

router.post('/', driverMiddleware, addTruck);

router.get('/', driverMiddleware, getTrucks);

router.get('/:id', driverMiddleware, getTruck);

router.put('/:id', driverMiddleware, updateTruck);

router.delete('/:id', driverMiddleware, deleteTruck);

router.post('/:id/assign', driverMiddleware, assignTruck);

module.exports = {
  trucksRouter: router,
};
