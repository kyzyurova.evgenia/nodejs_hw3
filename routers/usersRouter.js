const express = require('express');
const router = express.Router();
const { deleteUser, changePassword, getUser } = require('../controllers/usersController.js');
const { authMiddleware } = require('../middleware/authMiddleware.js');

router.get('/me', authMiddleware, getUser);

router.patch('/me/password', authMiddleware, changePassword);

router.delete('/me', authMiddleware, deleteUser);



module.exports = {
  usersRouter: router,
};
