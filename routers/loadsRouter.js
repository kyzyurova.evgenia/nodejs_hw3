const express = require('express');

const router = express.Router();

const { getLoads, addLoad, getActiveLoads, changeLoadState, getLoad, updateLoad, deleteLoad, postLoadById, shippingInfo } = require('../controllers/loadsController.js');

const { authMiddleware, shipperMiddleware, driverMiddleware } = require('../middleware/authMiddleware');

router.get('/', authMiddleware, getLoads);

router.post('/', shipperMiddleware, addLoad);

router.get('/active', driverMiddleware, getActiveLoads);

router.patch('/active/state', driverMiddleware, changeLoadState);

router.get('/:id', shipperMiddleware, getLoad);

router.put('/:id', shipperMiddleware, updateLoad);

router.delete('/:id', shipperMiddleware, deleteLoad);

router.post('/:id/post', shipperMiddleware, postLoadById);

router.get('/:id/shipping_info', shipperMiddleware, shippingInfo);


module.exports = {
  loadsRouter: router,
};
