const { User } = require('../models/Users.js');
const bcryptjs = require('bcryptjs');

function getUser (req, res, next) {
    User.findById(req.user.userId)
        .then((user) => {
            try{
                if(!user){
                    throw new Error('User was not found');
                }
            }catch(err){
                res.status(400).send({ message: err.message });
            }
          res.json({message: 'Success', user});
        });
}

const changePassword = async(req, res, next) => {
  
  try{
    const { oldPassword, newPassword } = req.body;
      if(!oldPassword){
          throw new Error('Old Password is empty')
      }else if(!newPassword){
        throw new Error('New Password is empty')
    }
    const password = await bcryptjs.hash(newPassword, 10);
    
    const user = await User.findById(req.user.userId);
    if(!user){
      throw new Error('User is not registered')
    }
    const isPassCorrect = await bcryptjs.compare(String(oldPassword), String(user.password));
    if(!isPassCorrect){
      throw new Error('Password is not correct')
    }
      User.findByIdAndUpdate({_id: req.user.userId}, {$set: { password:  password} })
          .then((result) => {
            try{
                if(result === null){
                    throw new Error('User was not found');
                }else{
                    res.json({message: 'Password changed successfully'});
                }
            }catch(err){
                res.status(400).send({ message: err.message });
            }
              
          });
      }catch(err){
          res.status(400).send({ message: err.message });
      }
  
}


const deleteUser = (req, res, next) => {
      User.findByIdAndDelete({_id: req.user.userId})
          .then((result) => {
        try{
              if(result === null){
                  throw new Error('Note was not found');
              }else{
                  res.json({message: 'Success'});
              }
            }catch(err){
                res.status(400).send({ message: err.message });
            }
        });
      
}

module.exports = {
    getUser,
    changePassword,
    deleteUser
  };