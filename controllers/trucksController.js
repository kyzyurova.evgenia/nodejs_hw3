const { Truck } = require('../models/Trucks.js');


function addTruck(req, res, next) {
  try{
    const { type } = req.body;
    if(!type){
      throw new Error('Type is empty');
    }
    const truck = new Truck({
      created_by : req.user.userId,
      type
    });
    truck.save()
      .then(saved => res.json({message: 'Success'}));
  }catch(err){
    res.status(400).send({ message: err.message });
  }
  
}
const getTrucks = (req, res, next) => {
  
    Truck.find({created_by: req.user.userId}, '-__v')
    .then((trucks) => {
      try{
        if(trucks.length === 0){
          throw new Error('Trucks was not found');
        }
        res.json({trucks: trucks});
      }catch(err){
        res.status(400).send({ message: err.message });
      }
      
    });
  
  
}

const getTruck = (req, res, next) => {
  
    Truck.find({_id: req.params.id, created_by: req.user.userId})
    .then((truck) => {
      try{
        if(truck === null){
          throw new Error('Truck was not found');
        }
        res.json({truck: truck});
      }catch(err){
        res.status(400).send({ message: err.message });
      }
      
    });
  
  
}
const deleteTruck = (req, res, next) => {
    Truck.findOneAndDelete({_id: req.params.id, created_by: req.user.userId})
    .then((truck) => {
      try{
        if(truck === null){
          throw new Error('Truck was not found');
        }
        res.json({message: 'Truck deleted successfully'});
      }catch(err){
        res.status(400).send({ message: err.message });
      }
    });
}
const updateTruck = (req, res, next) => {
  try{
    const { type } = req.body;
    if(!type){
      throw new Error('Type is empty');
    }
    Truck.find({_id: req.params.id, created_by: req.user.userId}).update({$set: { type } })
      .then((truck) => {
        try{
          if(truck === null){
            throw new Error ('Truck was not found')
          }
          res.json({message: 'Truck details changed successfully'});
        }catch(err){
          res.status(400).send({ message: err.message });
        }
        
      });
  }catch(err){
    res.status(400).send({ message: err.message });
  }
  
}
const assignTruck = (req, res, next) => {
  Truck.find({assigned_to: req.user.userId})
    .then((trucks) => {
      try{
        if(trucks.length !== 0){
          throw new Error('Driver has another truck')
        }
        Truck.findOneAndUpdate({_id: req.params.id, created_by: req.user.userId}, {$set: { assigned_to: req.user.userId } })
        .then((result) => {
          try{
            if(!result){
              throw new Error('Truck does not exist');
            }
            res.json({message: 'Truck assigned successfully'});
          }catch(err){
            res.status(400).send({ message: err.message });
          }
          
        });
      }catch(err){
        res.status(400).send({ message: err.message });
      }
    });
}

module.exports = {
  addTruck,
  getTrucks, 
  getTruck, 
  updateTruck, 
  deleteTruck, 
  assignTruck
};
