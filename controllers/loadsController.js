const { Load } = require('../models/Loads.js');
const { Truck } = require('../models/Trucks.js');

function addLoad(req, res, next) {
  const { name, payload, pickup_address, delivery_address, dimensions } = req.body;
  const load = new Load({
    created_by: req.user.userId,
    name, 
    payload, 
    pickup_address, 
    delivery_address, 
    dimensions 
  });
  load.save().then((load) => {
    res.json({message: "Load created successfully", load});
  });
}
const getLoads = (req, res, next) => {
    if(req.user.role === "DRIVER"){
      const { status , limit = 10, offset = 0 } = req.query;
        let loads = Load.find({assigned_to: req.user.userId});
        console.log(loads);
        if(status){
          loads = loads.find({status: { $eq: status }})
        }
        loads = loads.find({status: { $in: ["ASSIGNED", "SHIPPED"] }});
        console.log(loads);
        loads.skip(offset).limit(limit)
          .then((newloads) => {
            console.log(newloads);
            try{
              if(newloads.length === 0){
                throw new Error('Loads were not found');
            }else{
              newloads.forEach(load => {
                const logs = [{message: `Load assigned to driver with id ${load.assigned_to}`, time: Date.now()}];
                load.logs = logs;
              });
              console.log(newloads);
              res.json({loads: newloads});
            }
            }catch(err){
              res.status(400).send({ message: err.message });
          }
              
          });
    }else if(req.user.role === "SHIPPER"){
        const { status , limit = 10, offset = 0 } = req.query;
        let loads = Load.find({created_by: req.user.userId});
        if(status){
          loads = loads.find({status: {$eq : status}} );
        }else{
          loads.skip(offset).limit(limit)
          .then((newloads) => {
            console.log(newloads);
            try{
              if(newloads.length === 0){
                throw new Error('Loads were not found');
            }else{
                newloads.forEach(load => {
                  const logs = [{message: `Load assigned to driver with id ${load.assigned_to}`, time: Date.now()}];
                  load.logs = logs;
                });
                
                res.json({loads: newloads});
            }
            }catch(err){
              res.status(400).send({ message: err.message });
          }
          });
        }
    }
    
  }

const getLoad = (req, res, next) => {
  
    Load.find({_id: req.params.id, created_by: req.user.userId})
    .then((load) => {
      try{
      if(load.length === 0){
        throw new Error('Load was not found');
      }else{
      const logs = [{message: `Load assigned to driver with id ${load.assigned_to}`, time: Date.now()}];
      load.logs = logs;
      
      res.json({load: load});
      }
    }catch(err){
      res.status(400).send({ message: err.message });
    }
  });
  
  
}


const deleteLoad = (req, res, next) =>{
  
    Load.find({_id: req.params.id, created_by: req.user.userId}).then(foundLoad => {
      try{
      if(foundLoad.status !== 'NEW'){
        throw new Error('This load is not new, can`t be deleted');
      }
    }catch(err){
      res.status(400).send({ message: err.message });
    }
    })
    Load.findByIdAndDelete(req.params.id)
    .then((load) => {
      try{
      if(load === null){
        throw new Error('Load was not found');
      }
      
      res.json({message: 'Load deleted successfully'});
    }catch(err){
      res.status(400).send({ message: err.message });
    }
    });
  
}


const updateLoad = (req, res, next) => {
  try{
    const { name, payload, pickup_address, delivery_address, dimensions } = req.body;
    if(!name || !payload || !pickup_address || !delivery_address || !dimensions){
      throw new Error('Some of the fields are empty');
    }
    Load.findById(req.params.id).then(foundLoad => {
      try{
        if(foundLoad.status !== 'NEW'){
          throw new Error('This load is not new, can`t be updated');
        }
        Load.findByIdAndUpdate({_id: req.params.id}, {$set: { name, payload, pickup_address, delivery_address, dimensions } })
          .then((load) => {
            try{
              if(load.length === 0){
                throw new Error('Load was not found');
              }
              res.json({message: 'Load details changed successfully'});
            }catch(err){
              res.status(400).send({ message: err.message });
            }
            
          });
      }catch(err){
        res.status(400).send({ message: err.message });
      }
      
    })
    
    
  }catch(err){
    res.status(400).send({ message: err.message });
  }
  
}
const changeLoadState = (req, res, next) => {

    Load.find({assigned_to: req.user.userId, status: {$ne: 'SHIPPED'}})
    .then((load)=>{
      try{
      if(load.length === 0){
        throw new Error('Load was not found');
      }
      if(load[0].state === "En route to Pick Up"){
        Load.findByIdAndUpdate({_id: load[0].id},{$set: {state: "Arrived to Pick Up"}})
        .then(load =>{
          Load.findById(load).then(load => res.json({message: `Load state changed to ${load.state}`}));
        });
      }else if(load[0].state === "Arrived to Pick Up"){
        Load.findByIdAndUpdate({_id: load[0].id}, {$set: {state: "En route to Delivery"}})
        .then(load =>{
          Load.findById(load).then(load => res.json({message: `Load state changed to ${load.state}`}));
        });
      }else if(load[0].state === "En route to Delivery"){
        Load.findByIdAndUpdate({_id: load[0].id}, {$set: {state: "Arrived to Delivery", status: "SHIPPED"}})
        .then(load =>{
          Truck.find({assigned_to: load.assigned_to}).updateOne({$set: {status: "IS"}})
          .then(truck => console.log(truck.status));
          Load.findById(load).then(load => res.json({message: `Load state changed to ${load.state}`}));
        });
      }
    }catch(err){
      res.status(400).send({ message: err.message });
    }
      
    });
    
  
}
const getActiveLoads = (req, res, next) => {
  Load.find({assigned_to: req.user.userId, status:  { $eq: 'ASSIGNED' }})
  .then((loads) => {
    try{
      if(loads.length === 0){
        throw new Error('Active loads were not found');
    }else{
      loads.forEach(load => {
        const logs = [{message: `Load assigned to driver with id ${load.assigned_to}`, time: Date.now()}];
        load.logs = logs;
      });
        res.json({load: loads[0]});
    }
  }catch(err){
    res.status(400).send({ message: err.message });
  }
    
  });
}

const postLoadById = (req, res, next) => {
  try{
    if(!req.params.id){
      throw new Error ('id is missed')
    }
  
  Load.findByIdAndUpdate({_id: req.params.id}, {$set: { status: "POSTED" } })
    .then((load) => {
      console.log(load);
      try{
        if(load === null){
          throw new Error ('Load does not exist');
        }
        Truck.find({assigned_to: {$ne: null}, status:  { $eq: 'IS' }})
      .then(truck => {
        console.log(truck);
        try{
          if(truck.length === 0){
            Load.findByIdAndUpdate({_id: req.params.id}, {$set: { status: "NEW"} })
            throw new Error ('Truck does not exist');
          }
          console.log(truck[0]);

        Truck.findByIdAndUpdate({_id: truck[0].id}, {$set: {status: "OL"}})
        .then(truck => {
          Load.findByIdAndUpdate({_id: load.id}, {$set: {state: "En route to Pick Up", status: "ASSIGNED", assigned_to: truck.assigned_to}})
          .then(load =>res.json({message: 'Load posted successfully', driver_found: true}))
          
        })
        
        }catch(err){
          res.status(400).send({ message: err.message });
        }
      });
    }catch(err){
      res.status(400).send({ message: err.message });
    }
  });
  }catch(err){
    res.status(400).send({ message: err.message });
  }
}

const shippingInfo = (req, res, next) =>{
  
    Load.findById(req.params.id)
    .then(foundLoad => {
      try{
        if(foundLoad === null){
          throw new Error('Load does not exist');
        }
        Truck.find({assigned_to: foundLoad.assigned_to})
        .then(truck => {
          try{
            if(truck.length === 0){
              throw new Error('Truck does not exist');
            }
            res.json({load: foundLoad, truck: truck});
          }catch(err){
            res.status(400).send({ message: err.message });
          }
          
        });
      }catch(err){
        res.status(400).send({ message: err.message });
      }
    });
  
}

module.exports = {
  getLoads, 
  addLoad, 
  getLoad,
  getActiveLoads, 
  changeLoadState,
  updateLoad, 
  deleteLoad, 
  postLoadById, 
  shippingInfo
};
