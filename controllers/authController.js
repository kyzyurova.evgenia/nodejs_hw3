const { User } = require('../models/Users.js');
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');

const registerUser = async (req, res, next) => {
  
  try{
    const { email, password, role } = req.body;
    if(!email){
      throw new Error('Username is empty')
    }else if(!password){
      throw new Error('Password is empty')
    }else if(!role){
      throw new Error('Role is empty')
    }
    const user = new User({
      email,
      password: await bcryptjs.hash(password, 10),
      role
    });
  
    user.save()
      .then(saved => res.json({saved, message: 'Success'}))
      .catch(err => {
        next(err);
      });
  }catch(err){
    res.status(400).send({ message: err.message });
  }
  
}

const forgotPassword = async (req, res, next) => {
  try{
    const userEmail = req.body.email;
    if(!userEmail){
      throw new Error('Email is empty')
    }
    const user = await User.findOne({ email: userEmail });
    if(!user){
      throw new Error('User is not registered')
    }
    return res.json({message: 'New password sent to your email address'});
  }catch(err){
    res.status(400).send({ message: err.message });
  }
}

const loginUser = async (req, res, next) => {
  try{
    const userEmail = req.body.email;
    const userPass = req.body.password;
    if(!userEmail){
      throw new Error('Email is empty')
    }else if(!userPass){
      throw new Error('Password is empty')
    }
    const user = await User.findOne({ email: userEmail });
    if(!user){
      throw new Error('User is not registered')
    }
    const isPassCorrect = await bcryptjs.compare(String(userPass), String(user.password));
    if(!isPassCorrect){
      throw new Error('Password is not correct')
    }
    if (user && isPassCorrect) {
      const payload = { email: user.email, role: user.role, userId: user._id };
      const jwtToken = jwt.sign(payload, 'secretTokenKey');
      return res.json({message: 'Success', jwt_token: jwtToken});
    }
    return res.status(403).json({'message': 'Not authorized'});
  }catch(err){
    console.error(err.message);
    res.status(400).send({ message: err.message });
  }
    
}

module.exports = {
  registerUser,
  loginUser,
  forgotPassword
};
